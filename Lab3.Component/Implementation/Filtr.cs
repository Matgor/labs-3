﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;
namespace Lab3.Implementation
{
    class Filtr : IFiltruj
    {
        public void Filtruj()
        {
            Console.WriteLine("Filtr działa - filtruje");
        }

        public void WylaczFiltr()
        {
            Console.WriteLine("Filtr wyłaczony");
        }
    }
}
