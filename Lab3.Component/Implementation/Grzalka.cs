﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    class Grzalka : IGrzej
    {
        public void Grzej()
        {
            Console.WriteLine("Grzałka działa - grzeje kawe");
        }

        public void WylaczGrzalke()
        {
            Console.WriteLine("Grzałka wylaczona");
        }
    }
}
