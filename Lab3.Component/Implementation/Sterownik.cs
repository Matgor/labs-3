﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    public class Sterownik : IFiltruj, IGrzej, IMiel
    {
        Filtr filtr;
        Grzalka grzalka;
        Młynek mlynek;

        public Sterownik()
        {
            filtr = new Filtr();
            grzalka = new Grzalka();
            mlynek = new Młynek();
        }

        public void Start()
        {
            filtr.Filtruj();
            filtr.WylaczFiltr();
            mlynek.ZmielKawe();
            mlynek.WylaczMlynek();
            grzalka.Grzej();
            grzalka.WylaczGrzalke();
        }

        public void Filtruj()
        {
            throw new NotImplementedException();
        }

        public void WylaczFiltr()
        {
            throw new NotImplementedException();
        }

        public void Grzej()
        {
            throw new NotImplementedException();
        }

        public void WylaczGrzalke()
        {
            throw new NotImplementedException();
        }

        public void ZmielKawe()
        {
            throw new NotImplementedException();
        }

        public void WylaczMlynek()
        {
            throw new NotImplementedException();
        }
    }
}
