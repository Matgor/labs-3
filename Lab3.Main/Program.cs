﻿using System;
using Lab3.Contract;
using Lab3.Implementation;
namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Sterownik sterownik = new Sterownik();
            sterownik.Start();
        }
    }
    public static class MixTest
    {
        public static void Grzej(this IGrzej test)
        {
            test.Grzej();
        }
    }
}
